from datetime import datetime

from django.db import models
from django_ca.models import CertificateAuthority, Certificate

from django_mdat_customer.django_mdat_customer.models import MdatCustomers


class PkiFileStorage(models.Model):
    bytes = models.TextField()
    filename = models.CharField(max_length=255)
    mimetype = models.CharField(max_length=50)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "pki_file_storage"


class PkiFile(models.Model):
    name = models.CharField(max_length=255)
    picture = models.ImageField(
        upload_to="django_pki_manager_data.PkiFileStorage/bytes/filename/mimetype",
        blank=True,
        null=True,
    )
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "pki_file"


class PkiOwner(models.Model):
    ca = models.OneToOneField(CertificateAuthority, models.DO_NOTHING, primary_key=True)
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "pki_owner"


class PkiCertificatePrivateKey(models.Model):
    certificate = models.OneToOneField(Certificate, models.DO_NOTHING, primary_key=True)
    private_key = models.TextField()
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "pki_certificate_private_key"
