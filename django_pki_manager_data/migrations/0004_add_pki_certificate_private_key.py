# Generated by Django 3.2.13 on 2022-06-07 14:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_ca", "0026_auto_20210501_1258"),
        ("django_pki_manager_data", "0003_add_db_table"),
    ]

    operations = [
        migrations.CreateModel(
            name="PkiCertificatePrivateKey",
            fields=[
                (
                    "certificate",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        primary_key=True,
                        serialize=False,
                        to="django_ca.certificate",
                    ),
                ),
                ("private_key", models.TextField()),
            ],
            options={
                "db_table": "pki_certificate_private_key",
            },
        ),
    ]
