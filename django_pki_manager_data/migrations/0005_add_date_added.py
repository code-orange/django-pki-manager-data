# Generated by Django 3.2.13 on 2022-06-07 18:16

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_pki_manager_data", "0004_add_pki_certificate_private_key"),
    ]

    operations = [
        migrations.AddField(
            model_name="pkicertificateprivatekey",
            name="date_added",
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name="pkifile",
            name="date_added",
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name="pkifilestorage",
            name="date_added",
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name="pkiowner",
            name="date_added",
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
